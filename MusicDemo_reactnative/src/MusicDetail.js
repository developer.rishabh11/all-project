import React, {useState, useEffect} from 'react'
import { Image, ScrollView, StyleSheet, Text, View, FlatList, Touchable, TouchableOpacity } from "react-native";
import { useRoute } from '@react-navigation/native';
import { useNavigation } from '@react-navigation/native';
import TrackPlayer, {useProgress} from 'react-native-track-player';
import Slider from 'react-native-slider';
// import playIcon from '../../../assets/play-button-arrowhead.png';
import playIcon from "../assets/play-button-arrowhead.png";
import pauseIcon from '../assets/pause2.png';
import leftarrow from '../assets/left-arrow.png'

function MusicDetail(props){
  const param = props?.route?.params;
  // const navigation = useNavigation();
    // const route = useRoute();
    // route.params
    const [isPlaying, setIsPlaying] = useState(false);
  const [sliderValue, setSliderValue] = useState(0);

  const {position, duration} = useProgress();
  useEffect(() => {
    console.log("image inside details",param?.imageUrl)
    console.log("audio",param?.audiosongUrl)
   
    if(param?.audiosongUrl){
      setupPlayer();
    }
   
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      if (isPlaying && position < duration) {
        // Update the slider position every 250ms while playing
        // If the audio is not playing or has reached the end, stop the interval
        TrackPlayer.getPosition().then(currentPosition => {
          // We need to check if the audio is still playing to avoid updating the slider position after it has ended.
          if (currentPosition !== position) {
            // setPosition is a function to update the slider position.
            // Make sure you have useState for position and setPosition.
            setSliderValue(currentPosition);
          }
        });
      }
    }, 250);

    return () => clearInterval(interval);
  }, [isPlaying, position, duration]);

  const setupPlayer = async () => {
    await TrackPlayer.setupPlayer();
    await TrackPlayer.add({
      id: 'trackId',
      url: param?.audiosongUrl,
      title: 'Track Title',
      artist: 'Artist Name',
      artwork: 'http://example.com/track.jpg',
    });
  };

  const onSliderValueChange = value => {
    TrackPlayer.seekTo(value);
    setSliderValue(value);
  };

  const handlePlay = async () => {
    await TrackPlayer.play();
  };
  const handlePause = async () => {
    await TrackPlayer.pause();
  };

  return (
      <>
        <View style={style.mainContainer}>
          {/* <View style={style.mainContainerBack}> */}
          {/* <TouchableOpacity
                    onPress={() => {
                      navigation.goBack();
                    }}>
            <Image source={leftarrow} style={style.backBtn}/>
            </TouchableOpacity> */}
          {/* </View> */}
          <View style={style.containerImage} onPress={()=> console.log("Image click")}>
            <Image style={style.musicImage} source={{uri: param?.imageUrl}} />
          </View>
          <View style={style.musicNameContainer}>
            <Text style={style.musicTitle}>{param?.musicTitle}</Text>
            <Text style={style.musicBy}>{param?.authorName}</Text>
          </View>
          <View style={style.Player}></View>
          <View>
            <Slider
              style={{width: 300, height: 50}}
              minimumValue={0}
              maximumValue={duration}
              value={position}
              minimumTrackTintColor="#f6ca47"
              maximumTrackTintColor="#DBDBDB"
              thumbTintColor="#f6ca47"
              onValueChange={onSliderValueChange}
            />
          </View>
          <View style={style.flexbutton}>
            <TouchableOpacity onPress={handlePause}>
            <View style={style.borderContainer}>
                <Image source={pauseIcon} style={style.playicon} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={handlePlay}>
              <View style={style.borderContainer}>
                <Image source={playIcon} style={style.playicon} />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </>
    )
}

const style = StyleSheet.create({
  playicon: {
    width: 20,
    height: 20,
  },
  mainContainerBack:{
    textAlign:'left',
    position:'absolute',
    left:20
  },
  backBtn:{
    width:20,
    height:20,
    marginTop:30,
    alignItems:'left'
  },
  borderContainer: {
    borderWidth: 2,
    padding: 15,
    borderRadius: 100,
    borderColor: 'black',
    margin:10,
    textAlign:'center'
  },
  mainContainer: {
    width: '100%',
    margin: 'auto',
    alignItems: 'center',
    height: '100%',
    backgroundColor: 'white',
  },
  containerImage: {
    marginTop: 70,
    // width: 'auto',
    width: 300,
    height: 300,
    backgroundColor:'red'
  },
  musicImage: {
    borderRadius: 10,
    height: '100%'
  },
  musicNameContainer: {
    marginTop: 20,marginLeft:15,marginRight:15
  },
  musicTitle: {
    color: '#1B1B1B',
    fontSize: 24,
    fontWeight: '700',
  },
  musicBy: {
    color: '#939393',
    fontSize: 15,
    textAlign: 'center',
    marginTop: 3,
    fontWeight: '400',
  },
  flexbutton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  padding: {
    marginHorizontal: 20,
  },
});
export default MusicDetail