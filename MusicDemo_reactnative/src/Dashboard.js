import React, { useState, useEffect } from 'react'
import { Image, ScrollView, StyleSheet, Text, View, FlatList, ActivityIndicator, TouchableOpacity } from "react-native";
import { heightPercentageToDP } from 'react-native-responsive-screen';
import { useNavigation } from '@react-navigation/native';
import SQLite from 'react-native-sqlite-storage';

function Dashboard() {
  const navigation = useNavigation();
  const baseUrl = 'http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=25/json'
  const [res, setRes] = useState({
    err: true,
    data: [
      {
        id: '',
        title: '',
        authorName: '',
        imageUrl: '',
        audio: ''
      }
    ]
  });
  const [loading, setLoading] = useState(true);
  //Open the database
  const db = SQLite.openDatabase(
    {
      name: 'topsong.db',
      location: 'default',
      //createFromLocation: '~www/myDatabase.db',
    },
    () => {
      console.log('Database opened successfully.');
    },
    error => {
      console.error('Failed to open database:', error);
    },
  );
  const fetchData = () => {
     // Create a table
     db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS topsongs (id INTEGER PRIMARY KEY, title TEXT, authorName TEXT, imageUrl TEXT,audio TEXT)',
        [],
        (tx, results) => {
          console.log('Table created');
        },
        (error) => {
          console.error('Error creating table:', error);
        }
      );
    });
    // Query data from the table
    db.transaction((tx) => {
      tx.executeSql('SELECT * FROM topsongs', [], (tx, results) => {
        const topsongs = results.rows.raw();
        if (topsongs.length > 0) {
          console.log("Data fetched from SQLite");
          const songs = [];
          topsongs.map((item) => {
              songs.push(
                {
                  id: item.id,
                  title: item.title,
                  authorName: item.authorName,
                  imageUrl: item.imageUrl,
                  audio: item.audio
                }
              )
          });
          setRes({ err: false, data: songs });
          setLoading(false);
        }else{
          getApiData();
        }
      });
    });
  }

  const getApiData = async () => {
    console.log("Fetching data from API");
    await fetch(baseUrl,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      }).then
      (response => response.json()).then(json => {
        setLoading(false);
        const songs = []
        json?.feed?.entry.map((item, index) => {
          const data = item['link'].find((d) => d['attributes']?.type == 'audio/x-m4a');
          if (data) {
            songs.push(
              {
                id: index,
                title: item['title']?.label,
                authorName: item['im:name']?.label,
                imageUrl: item['im:image'][2]?.label,
                audio: data.attributes.href
              }
            )
          }
        });
        setRes({ err: false, data: songs });

        // Insert data into the table
        db.transaction((tx) => {
          songs.map((song, index) => {
            tx.executeSql('INSERT INTO topsongs (id, title, authorName, imageUrl, audio) VALUES (?, ?, ?, ?, ?)', [index, song.title, song.authorName, song.imageUrl, song.audio]);
          })
        });
        return json;
      }).catch(error => {
        setRes({ err: true, data: error });
        setLoading(false);
        console.error(error);
      });
  }

  useEffect(() => {
    console.log("real");
    fetchData();
  }, [])


  return (
    <>
      {loading ?
        <ActivityIndicator />
        :
        <View>
          {!res.err && res?.data?.length > 0 ?
            <FlatList
              style={styles.flatlistStyle}
              data={res.data}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  onPress={() => {
                    // const data = item['link'].map((d) => { if (d['attributes']?.type == 'audio/x-m4a') return d });
                    // const data = item['link'].find((d) => d['attributes']?.type == 'audio/x-m4a');
                    // if (data) {
                      console.log(item);
                    navigation.navigate('MusicDetail', {
                      imageUrl: item.imageUrl,
                      musicTitle: item.title,
                      authorName: item.authorName,
                      audiosongUrl: item.audio
                    });
                    // }
                  }
                  }
                >
                  <View style={styles.container} key={index}>
                    <View style={styles.mainContaier}>
                      <View style={styles.mainContainerforImage}>
                        <Image
                          source={{ uri: item.imageUrl }}
                          style={styles.imageSize}
                        />
                      </View>
                      <View style={styles.headingContainer}>
                        <View>
                          <Text style={styles.heading}>
                            {item.authorName}
                          </Text>
                        </View>
                        <View>
                          <Text style={styles.subHeading}>
                            {item.title}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
              numColumns={2}
            /> : <Text>Something went Wrong</Text>
          }
        </View>

      }
    </>
  )
}

const styles = StyleSheet.create({
  flatlistStyle: {
    height: '100%',
    overflow: 'scroll',
    width: '100%',
  },
  container: {
    height: heightPercentageToDP('35%'),
    marginTop: 20,
  },
  mainContaier: {
    backgroundColor: 'white',
    borderWidth: 0,
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#808080',
    width: 160,
    elevation: 10,
    borderRadius: 10,
    height: '100%',
  },
  mainContainerforImage: {
    // width: 100,
    height: 150,
  },
  imageSize: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  heading: {
    fontSize: 14,
    color: 'black',
    fontWeight: '800',
    marginTop: 0,
  },
  subHeading: {
    fontSize: 12,
    color: '#939393',
    fontWeight: '400',
    marginTop: 2,
  },
  headingContainer: {
    padding: 10,
  }
});
export default Dashboard