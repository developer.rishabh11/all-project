package main

import (
	"log"
	"practice/helper"
)

const numPool = 1000

func main() {
	intChain := make(chan int)
	defer close(intChain)

	// close channel
	go calculateNumber(intChain) // background thread

	num := <-intChain // main thread
	log.Println(num)
}

func calculateNumber(intChain chan int) {
	randomNumber := helper.RandomNumber(numPool)
	intChain <- randomNumber
}
