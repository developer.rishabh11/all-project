package helper

import (
	"log"
	"math/rand"
	"time"
)

func RandomNumber(n int) int {
	log.Println("value", n)
	rand.Seed(time.Now().UnixNano())
	value := rand.Intn(n)
	return value
}
