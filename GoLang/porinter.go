package main

import "log"

func main() {
	var myString string
	myString = "Green"
	log.Println("Mystring is set to", myString)
	chnageTheValue(&myString)
	log.Println("after func call myString set to", myString)
}

func chnageTheValue(s *string) {
	newValue := "Red"
	*s = newValue
}
