package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func Home(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "home.page.tmpl")
}
func About(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "about.page.tmpl")

}

func renderTemplate(w http.ResponseWriter, tmpl string) {
	paresedTemplate, _ := template.ParseFiles("./templates/" + tmpl)
	err := paresedTemplate.Execute(w, nil)
	if err != nil {
		fmt.Println("Error pasring template:", err)
		return
	}
}
