package main

import "log"

func main() {
	isTrue := true

	if isTrue {
		log.Println("istrue is", isTrue)
	} else {
		log.Println("isTrue is", isTrue)
	}

	switch isTrue {
	case true:
		log.Println("istrue is", isTrue)
		break
	case false:
		log.Println("isTrue is", isTrue)
		break
	}
}
