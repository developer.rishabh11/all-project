package main

import "log"

func main() {
	myMap := make(map[string]string, 0)
	myMap["dog"] = "katty"
	log.Println(myMap["dog"])

	var slic []string
	slic = append(slic, "data")
	slic = append(slic, "name")
	log.Println("myslic", slic)
	log.Println(slic[0:2])

	names := []int{1, 2, 3, 4, 5}
	log.Println(names)
}
