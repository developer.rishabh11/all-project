package main

import "log"

var s = "seven"

type User struct {
	FirstName string
	LastName  string
	Age       int
}

var name string //private
var New string  // public

func main() {
	var s2 = "six"
	log.Println(s)
	log.Println("s2 is", s2)

	user := User{
		FirstName: "John Doe",
		LastName:  "no",
	}

	log.Println(user.returnName())
}

// struct function receiver
func (u *User) returnName() string {
	return u.FirstName
}
