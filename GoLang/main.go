package main

import "fmt"

var youProgram string //package level variable

func main() {
	fmt.Println("Hello, world")
	// variable
	var whatToSay string //string
	whatToSay = "Good morning, All"
	fmt.Println(whatToSay)

	var i int // int
	i = 10
	fmt.Println("i is set to ", i)

	whatWasSaid := saySomething() //calling function
	fmt.Println("The function is", whatWasSaid)

	whatWasSaid, whatIsOtherThing := sayElse()
	fmt.Printf("%s and %v\n", whatWasSaid, whatIsOtherThing)
}

func saySomething() string { //func returning something
	return "Saying something"
}

func sayElse() (string, string) { //func returning two type
	return "Saying something", "nothing"
}
