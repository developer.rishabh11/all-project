package main

import (
	"fmt"
	"net/http"
)

// func main() {
// 	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
// 		n, err := fmt.Fprintf(w, "hello, world")

// 		if err != nil {
// 			fmt.Println(err)
// 		}
// 		fmt.Println(fmt.Sprintf("Number of bytes written: %d", n))
// 	})

// 	_ = http.ListenAndServe(":8080", nil)
// }

// function and handler                    function and handler                function and handler

// const portNumber string = ":8080"

// func Home(w http.ResponseWriter, r *http.Request) {
// 	_, _ = fmt.Fprintf(w, "This is a home page")
// }
// func About(w http.ResponseWriter, r *http.Request) {
// 	sum := addValue(4, 4)
// 	_, _ = fmt.Fprintf(w, "This is a about page addValue result is:- %d", sum)
// }

// func addValue(x, y int) int {
// 	return x + y
// }

// func main() {
// 	http.HandleFunc("/", Home)
// 	http.HandleFunc("/about", About)

// 	fmt.Println(fmt.Sprintf("Starting application on port %s", portNumber))

// 	_ = http.ListenAndServe(portNumber, nil)
// }

const portNumber = ":8080"

func main() {
	http.HandleFunc("/", Home)
	http.HandleFunc("/about", About)

	fmt.Println(fmt.Sprintf("Starting application on port %s", portNumber))

	_ = http.ListenAndServe(portNumber, nil)
}
